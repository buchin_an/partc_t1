package com.company;

import java.util.Scanner;

public class Main {

    private static String stringMIN;
    private static int lengthMIN;
    private final static int COUNT = 5;

    public static void main(String[] args) {

        String[] strings = new String[COUNT];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < COUNT; i++) {
            strings[i] = scanner.nextLine();

            if (stringMIN == null || lengthMIN > strings[i].length()) {
                lengthMIN = strings[i].length();
                stringMIN = strings[i];
            }
        }
        System.out.println(lengthMIN);
        System.out.println(stringMIN);

    }
}
